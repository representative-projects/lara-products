<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;

class ApiController extends Controller
{

    public function getProduct( $id )
    {
        $product = Products::findOrFail( $id );

        return response()
            ->json( [ 'product' => $product->toArray(), 'attributes' => $product->productAttributes->toArray() ] )
            ->header( 'Content-Type', 'application/json' );
    }

    public function getProducts()
    {
        return response()->json( [ 'products' => Products::all()->toArray() ] );
    }
}
