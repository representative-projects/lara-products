<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProducts;
use App\Models\Products;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'products.index', [ 'products' => Products::all() ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'products.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProducts $request
     * @return \Illuminate\Http\Response
     */
    public function store( StoreProducts $request )
    {
        $validated                  = $request->validated();
        $product                    = new Products();

        $product->fill( $validated );
        $product->save();

        session()->flash( 'status', 'New Product created!' );
        return redirect()->route( 'products.show', [ 'product' =>$product->id ] );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        return view( 'products.show', [ 'product' => Products::findOrFail( $id ) ] );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
        return view( 'products.edit', [ 'product' => Products::findOrFail( $id ) ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreProducts $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( StoreProducts $request, $id )
    {
        $validated                  = $request->validated();
        $product                    = Products::findOrFail( $id );

        $product->fill( $validated );
        $product->save();

        session()->flash( 'status', 'Product updated!');
        return redirect()->route( 'products.show', [ 'product' => $product->id ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        $product = Products::findOrFail( $id );
        $product->delete();

        session()->flash( 'status', 'Product deleted!');
        return redirect()->route( 'products.index' );
    }
}
