@extends( 'layouts.app' )

@section( 'title', 'Products List' )

@section( 'content' )
    <div class="container">
        <div class="list-group list-group-horizontal-md mb-sm-2 mb-md-3 mb-lg-4">
            <a class="list-group-item list-group-item-action list-group-item-primary text-center col-sm-12 col-md-4 col-lg-3" href="{{ route( 'products.create') }}">Add New Product</a>
        </div>
        <div class="cl-sm-12 cl-md-12 cl-lg-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Created</th>
                        <th scope="col">Updated</th>
                        <th scope="col">Details</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $products as $product )
                        <tr>
                            <th scope="row">{{ $product->id }}</th>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->description }}</td>
                            <td>{{ $product->created_at }}</td>
                            <td>{{ $product->updated_at }}</td>
                            <td><a href="{{ route('products.show', [ 'product' => $product->id ]) }}">Details</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
