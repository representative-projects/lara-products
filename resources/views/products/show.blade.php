@extends( 'layouts.app' )

@section( 'title', 'Product' )

@section( 'content' )
    <div class="container">
        <div class="list-group list-group-horizontal-md mb-sm-2 mb-md-3 mb-lg-4">
            <div class="list-group list-group-horizontal-md col-md-3 col-lg-2">
                <a class="list-group-item list-group-item-action list-group-item-primary text-center col-md-5" href="{{ route( 'products.index' ) }}">Back</a>
                <a class="list-group-item list-group-item-action list-group-item-warning text-center col-md-7" href="{{ route( 'products.edit', [ 'product' => $product->id ] ) }}">Edit</a>
            </div>
            <div class="list-group list-group-horizontal-md col-md-3">
                <form class="" method="POST" action="{{ route( 'products.destroy', [ 'product' => $product->id ]) }}">
                    @csrf
                    @method('DELETE')
                    <div>
                        <button class="list-group-item list-group-item-action list-group-item-danger text-center rounded-sm" type="submit">Delete</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="card bg-light mb-3 col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="card-header bg-transparent">Product #{{ $product->id }}</div>
                <div class="card-body">
                    <h5 class="card-title">Product Name</h5>
                    <p class="card-text">{{ $product->name }}</p>
                    <h5 class="card-title">Product Description</h5>
                    <p class="card-text">{{ $product->description }}</p>
                </div>
                <div class="card-footer bg-transparent">
                    <span>Created: {{ $product->created_at }}</span>
                    <br>
                    <span>Updated: {{ $product->updated_at }}</span>
                </div>
            </div>


            @if( $product->productAttributes->count() > 0 )
                @foreach( $product->productAttributes as $attribute )
                    <div class="card mb-3 col-12 col-sm-12 col-md-6 col-lg-4">
                        <div class="card-header bg-transparent">Product Attribute #{{ $attribute->id }}</div>
                        <div class="card-body">
                            <h5 class="card-title">{{ $attribute->key }}</h5>
                            <p class="card-text">{{ $attribute->value }}</p>
                        </div>
                        <div class="card-footer bg-transparent">
                            <span>Created: {{ $attribute->created_at }}</span>
                            <br>
                            <span>Updated: {{ $attribute->updated_at }}</span>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection
