<div class="form-row">
    <div class="col-md-4 mb-4">
        <label for="product_name">Product Name</label>
        <input class="form-control @error('name') is-invalid @enderror" id="product_name" type="text" name="name" value="{{ old( 'name', optional( $product ?? null )->name ) }}">
        @error('name')
        <div class="invalid-feedback">
            <span>{{ $message }}</span>
        </div>
        @enderror
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-4">
        <label for="product_description">Product Description</label>
        <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="product_description" >{{ old( 'description', optional( $product ?? null )->description ) }}</textarea>
        @error('description')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>
</div>
