@extends( 'layouts.app' )

@section( 'title', 'Edit Product' )

@section( 'content' )
    <div class="container">
        <div class="list-group list-group-horizontal-md mb-sm-2 mb-md-3 mb-lg-4">
            <a class="list-group-item list-group-item-action list-group-item-primary text-center col-sm-12 col-md-3 col-lg-1" href="{{ route( 'products.show', [ 'product' => $product->id] ) }}">Back</a>
        </div>
        <div>
            <form action="{{ route( 'products.update', [ 'product' => $product->id ] ) }}" method="POST">
                @csrf
                @method('PUT')
                @include( 'products.partials.form' )
                <div class="list-group">
                    <button class="list-group-item list-group-item-action list-group-item-success text-center col-sm-12 col-md-3 col-lg-2" type="submit">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
